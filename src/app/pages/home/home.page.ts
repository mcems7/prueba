import { Component, ViewChild, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
//import { Router,RouterLink } from '@angular/router';
import { UserService } from '../../api/user.service';
import { Platform, NavController } from '@ionic/angular';
// import { IUser } from '../../interfaces/interface.app';
import { IonInfiniteScroll } from '@ionic/angular';
import * as Constants from '../../constants';
import { HttpClient } from '@angular/common/http';
import { Response } from '../../interfaces/interface.app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild(IonInfiniteScroll, {static: false}) infiniteScroll: IonInfiniteScroll;

  // public items: IUser;
  public items: any[] = [];
  total: any;
  actual: any;
  page: any = 1;
  navigate: any;
  backButtonSubscription;
  paramsParaSegPag = {
    unTexto: "Hola",
    unNumero: 6238
  };
  //detailPage = "DetailPage";
  constructor(
    private http: HttpClient,
    private platform: Platform,
    private router: Router,
    private userService: UserService,
    public navCtrl: NavController,
    //public routerLink: RouterLink

    ) {
      this.loadData(false);
      
     }
     abrirDetailPag() {
       console.log('Detalles');

    }
  get_users(page = 1) {
    const url = `${Constants.API_ENDPOINT}/api/users?page=${page}`;
    return this.http.get<Response>(url);
  }

  loadData(event) {
          this.get_users(this.page).subscribe(
            data => {
              this.total = data.total;
              this.page = data.current_page + 1;
              this.items.push(...data.data);
              this.actual = this.items.length;
            });
          setTimeout(() => {
                if (event) {
                event.target.complete();
                }
                console.log('Done');
            // App logic to determine if all data is loaded
            // and disable the infinite scroll
                if (event) {
                  if (this.items.length == this.total) {
                    console.log('infinite deshabilitado');
                    event.target.disabled = true;
                  } else {
                    console.log('infinite habilitado');
                    event.target.disabled = false;
                  }
                }
          }, 500);
    }

    toggleInfiniteScroll() {
      this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }
    ngOnInit() { }
}
