import { Component, OnInit } from '@angular/core';
import { NavParams, NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { IUser } from '../../interfaces/interface.app';
import * as Constants from '../../constants';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
 // objetoRecibido: any;
//unTextoRecibido: string; 
  //unNumeroRecibido: number; 
  type: any;
  phones: any;
  user: IUser;
  constructor(
    public navCtrl: NavController, 
    public router : Router,
    private route: ActivatedRoute,
    private http: HttpClient
    //public navParams: NavParams
    ) {
      this.user = {
        name:'',
        address:'',
        photo:'/assets/icon/user.png'
      };
      this.phones = [];
      this.getparams();
    // this.unTextoRecibido = 2;//navParams.get("unTexto");
    // this.unNumeroRecibido = 5;//navParams.get("unNumero");
    //  this.objetoRecibido = navParams.data;
  }
  ngOnInit() {
    
  }
  get_user(id) {
    const url = `${Constants.API_ENDPOINT}/api/users/${id}`;
    return this.http.get<IUser>(url);
  }
  getparams() {
   
    this.route
    .queryParams
    .subscribe(params => {
      
     this.type = params.user || 0;
     //console.log(this.type);
      if (this.type!=0){
        this.get_user(this.type).subscribe(
          data => {
            this.user = data;
            this.phones = JSON.parse(data.phone);
            //console.log(data);
          }); 
      }
    });
  }
}
