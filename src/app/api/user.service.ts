import { Injectable } from '@angular/core';
import * as Constants from '../constants';
import { HttpClient } from '@angular/common/http';
import { Response } from '../interfaces/interface.app';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  get_users(page = 1) {
    const url = `${Constants.API_ENDPOINT}/api/users?page=${page}`;
    return this.http.get<Response>(url);
  }
}
