export interface Response {
    current_page:   number;
    data:           IUser[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    next_page_url:  string;
    path:           string;
    per_page:       number;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface IUser {
    id?: number;
    name?: string;
    photo?: string;
    phone?: string;
    address?: string;
    email?: string;
    password?: string;
    email_verified_at?: null | string;
    created_at?: string;
    updated_at?: string;
}
